﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace MoladekCS
{
    public class Steps
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(Steps));

		public Steps(){}

		public IWebDriver InitBrowser()
        {
			OpenQA.Selenium.Remote.DesiredCapabilities capability = OpenQA.Selenium.Remote.DesiredCapabilities.Firefox();
			OpenQA.Selenium.Remote.RemoteWebDriver wd = new OpenQA.Selenium.Remote.RemoteWebDriver(new Uri("http://127.0.0.1:4444/wd/hub"), capability);
			return wd;
        }

        public void LoginGithub(IWebDriver driver, string username, string password)
        {
            logger.Info("Log in to github");
            LoginPage loginPage = new LoginPage(driver);
            loginPage.OpenPage();
            loginPage.Login(username, password);
        }

		public bool IsLoggedIn(IWebDriver driver, string username)
        {
            LoginPage loginPage = new LoginPage(driver);
            return (loginPage.GetLoggedInUserName().Trim().ToLower().Equals(username));
        }

		public bool CreateNewRepository(IWebDriver driver, string repositoryName, string repositoryDescription)
        {
            logger.Info("Creating new repo with name: " + repositoryName);
            MainPage mainPage = new MainPage(driver);
            mainPage.ClickOnCreateNewRepositoryButton();
            CreateNewRepositoryPage createNewRepositoryPage = new CreateNewRepositoryPage(driver);
            string expectedRepoName = createNewRepositoryPage.CreateNewRepository(repositoryName, repositoryDescription);

            return expectedRepoName.Equals(createNewRepositoryPage.GetCurrentRepositoryName());
        }

		public bool CurrentRepositoryIsEmpty(IWebDriver driver)
        {
            CreateNewRepositoryPage createNewRepositoryPage = new CreateNewRepositoryPage(driver);
            return createNewRepositoryPage.IsCurrentRepositoryEmpty();
        }
    }
}
