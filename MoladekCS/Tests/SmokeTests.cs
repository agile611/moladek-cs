﻿	using NUnit.Framework;
	using OpenQA.Selenium.Support;
	using OpenQA.Selenium.Firefox;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Threading.Tasks;
	using OpenQA.Selenium;

	namespace MoladekCS
	{
	    [TestFixture]
	    public class SmokeTests
	    {
			private Steps steps = new Steps ();
	        private const string USERNAME = "testautomationuser";
	        private const string PASSWORD = "Time4Death!";
			private IWebDriver driver;

	        [SetUp]
	        public void Init()
	        {
				OpenQA.Selenium.Remote.DesiredCapabilities capability = OpenQA.Selenium.Remote.DesiredCapabilities.Firefox();
				OpenQA.Selenium.Remote.RemoteWebDriver wd = new OpenQA.Selenium.Remote.RemoteWebDriver(new Uri("http://127.0.0.1:4444/wd/hub"), capability);
				driver = wd;
	        }

	        [TearDown]
	        public void Cleanup()
	        {
				driver.Close ();
	        }

	        [Test]
	        public void OneCanLoginGithub()
	        {
	            steps.LoginGithub(driver, USERNAME, PASSWORD);
	            Assert.True(steps.IsLoggedIn(driver, USERNAME));
	        }

	        [Test]
	        public void OneCanCreateProject()
	        {
	            steps.LoginGithub(driver, USERNAME, PASSWORD);
	            Assert.IsTrue(steps.CreateNewRepository(driver, "testRepo", "auto-generated test repo"));
	            Assert.IsTrue(steps.CurrentRepositoryIsEmpty(driver));
	        }
	    }
	}
