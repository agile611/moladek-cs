﻿using log4net;
using log4net.Config;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GitHubAutomation.Driver;

namespace GitHubAutomation.Tests
{
    public class LogTest: BaseWebDriverTest
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(LogTest));
        private Steps.Steps steps = new Steps.Steps();
        private const string USERNAME = "testautomationuser";
		private const string PASSWORD = "password";

        [Test]
        public void OneCanTakeAScreenshot()
        {
            steps.InitBrowser();
            steps.LoginGithub(USERNAME, PASSWORD);
            //steps.TakeScreenshot();
            //steps.CloseBrowser();
        }
    }
}
